﻿using crud.Consultas;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestesCrud
{
    [TestClass]
    public class VendaProdutoConsultaTestes
    {
        [TestMethod]       
        public void TotalProduto_QuandoChamado_DeveRetornarTotalProduto()
        {
            //arrange
            var vendaProdutoConsulta = new VendaProdutoConsulta();
            vendaProdutoConsulta.preco = 5;
            vendaProdutoConsulta.quantidade = 5;
            var resultadoEsperado = 25;

            //act
            var resultadoAtual = vendaProdutoConsulta.totalProduto;

            //assert
            Assert.AreEqual(resultadoEsperado, resultadoAtual);
        } 
    }
}
