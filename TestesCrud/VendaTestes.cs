﻿using System;
using System.Collections.Generic;
using crud.Entidades;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CrudTestes
{
    [TestClass]
    public class VendaTestes
    {
        [TestMethod]
        public void Total_QuandoChamado_DeveRetornarTotalVenda()
        {

            //arrange
            var venda = new Venda();           
            var lista = new List<VendaProduto>();

            for (int i = 0; i < 5; i++)
            {
                var vendaProduto = new VendaProduto();
                vendaProduto.totalProduto = i;

                lista.Add(vendaProduto);
            }
            float resultadoEsperado = 10;
            venda.produtos = lista;

            //act
            var resultadoAtual = venda.total;

            //assert
            Assert.AreEqual(resultadoEsperado, resultadoAtual);
        }
    }
}
