﻿using crud.Entidades;
using crud.Repositorios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crud
{
    public partial class TelaCadastroUsuario : Form
    {
        public TelaCadastroUsuario()
        {
            InitializeComponent();
        }

        private void btnIrPaginaLogin_Click(object sender, EventArgs e)
        {
            TelaLogin telaLogin = new TelaLogin();
            telaLogin.Show();
            this.Close();
        }

        private void btnCriarConta_Click(object sender, EventArgs e)
        {

            if (txtUsuario.Text.Length < 3)
            {
                MessageBox.Show("Nome de Usuário muito curto! Insira ao menos 3 caracteres.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            } else if (txtSenha.Text.Length < 5)
            {
                MessageBox.Show("Senha muito curta! Insira ao menos 5 caracteres!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            } else
            {

                Usuario usuario = new Usuario();
                UsuarioRepositorio usuarioRepo = new UsuarioRepositorio();

                usuario.nomeUsuario = txtUsuario.Text;
                usuario.senha = txtSenha.Text;

                var verificaNomeUsuario = usuarioRepo.verificarSeNomeUsuarioExiste(usuario);

                if (verificaNomeUsuario == string.Empty)
                {
                    bool retorno = usuarioRepo.criarConta(usuario);

                    if (retorno)
                    {
                        MessageBox.Show("Cadastro realizado com sucesso! Vá para a página de login para acessar o sistema!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show(verificaNomeUsuario, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}
