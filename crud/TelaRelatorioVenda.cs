﻿using crud.Entidades;
using crud.Repositorios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using crud.Consultas;

namespace crud
{
    public partial class TelaRelatorioVenda : Form
    {
        public TelaRelatorioVenda()
        {
            InitializeComponent();
        }

        private void RelatorioVenda_Load(object sender, EventArgs e)
        {

            //dgvProdutos.Columns.Add("Codigo", "Codigo");
            dgvProdutos.Columns.Add("Nome", "Nome");
            dgvProdutos.Columns.Add("Preço", "Preço");
            dgvProdutos.Columns.Add("Quantidade", "Quantidade");
            dgvProdutos.Columns.Add("Total Produto", "Total Produto");

            VendaConsulta venda = new VendaConsulta();
            VendaProdutoRepositorio vendaProdutoRepo = new VendaProdutoRepositorio();
            VendaRepositorio vendaRepo = new VendaRepositorio();      

            int codigoVenda = vendaRepo.recuperarIdUltimaVenda();

            venda = vendaRepo.recuperarInfoUltimaVenda(codigoVenda);

            List<VendaProdutoConsulta> produtosVenda = vendaProdutoRepo.recuperarRelatorioProdutos(codigoVenda);

            //List<List<string>> produtosVenda = vendaProdutoRepo.recuperarRelatorioProdutos(codigoVenda);

            int qtdProdutosVenda = produtosVenda.Count;

            for (int i = 0; i < qtdProdutosVenda; i++) 
            {
                dgvProdutos.Rows.Add(produtosVenda[i].produto.nomeProduto, produtosVenda[i].produto.preco, produtosVenda[i].quantidade, produtosVenda[i].totalProduto);
            }

            txtNomeCliente.Text = venda.nomeCliente;
            txtObservacao.Text = venda.observacao;
            txtTotal.Text = venda.total.ToString();
            txtCodigoVenda.Text = venda.codigoVenda.ToString();
        }
    }
}
