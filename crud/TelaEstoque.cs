﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using crud.Entidades;
using crud.Repositorios;
using Crud;

namespace crud
{
    public partial class TelaEstoque : Form
    {
        bool naoNumeroDigitado = false;
        public TelaEstoque()
        {
            InitializeComponent();
        }

        private bool validarCampos()
        {
            if(txtCodigo.Text == string.Empty || 
                txtNome.Text == string.Empty ||
                txtMarca.Text == string.Empty ||
                txtQtdEstoque.Text == string.Empty ||
                txtAddAoEstoque.Text == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void btnBuscarPorId_Click(object sender, EventArgs e)
        {

            if(txtBuscarProduto.Text == string.Empty)
            {
                MessageBox.Show("Você precisa digitar um Id!", "Aviso");

            }
            else
            { 
                Produto produto = new Produto();
                ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

                int codigoProdutoBuscado = int.Parse(txtBuscarProduto.Text);

                produto = produtoRepo.buscarProdutoPorId(codigoProdutoBuscado);

                if(produto != null)
                {
                    txtCodigo.Text = produto.codigo.ToString();
                    txtNome.Text = produto.nome;
                    txtMarca.Text = produto.marca;
                    txtQtdEstoque.Text = produto.qtdEmEstoque.ToString();
                }

                
            }
        }
        private void btnAlterarEstoque_Click(object sender, EventArgs e)
        {

            if(validarCampos())
            {
            
                Produto produto = new Produto();
                ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

                bool retorno = produtoRepo.adicionarAoEstoque(int.Parse(txtCodigo.Text), float.Parse(txtAddAoEstoque.Text));

                if(retorno)
                {   
                    int codigoProdutoBuscado = int.Parse(txtBuscarProduto.Text);
                    produto = produtoRepo.buscarProdutoPorId(codigoProdutoBuscado);

                    if (produto != null)
                    {
                        txtCodigo.Text = produto.codigo.ToString();
                        txtNome.Text = produto.nome;
                        txtMarca.Text = produto.marca;
                        txtQtdEstoque.Text = produto.qtdEmEstoque.ToString();

                        txtAddAoEstoque.Text = string.Empty;
                    }
                }
            }
            else
            {
                MessageBox.Show("Selecione um produto antes de atualizar o estoque! Você pode fazer isso procurando por um produto no campo de busca no topo da tela.", "Aviso");
            }
        }

        private void btnLimparCampos_Click(object sender, EventArgs e)
        {
            txtCodigo.Text = string.Empty;
            txtNome.Text = string.Empty;
            txtMarca.Text = string.Empty;
            txtQtdEstoque.Text = string.Empty;

            txtAddAoEstoque.Text = string.Empty;
        }

        private void txtBuscarProduto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    if (e.KeyCode != Keys.Back)
                    {
                        naoNumeroDigitado = true;
                    }
                        
                }
            }
            else
            {
                naoNumeroDigitado = false;
            }
        }

        private void txtBuscarProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (naoNumeroDigitado)
            {
                e.Handled = true;
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
