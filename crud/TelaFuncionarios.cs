﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using crud.Entidades;
using crud.Repositorios;

namespace crud
{
    public partial class TelaFuncionarios : Form
    {
        public TelaFuncionarios()
        {
            InitializeComponent();
        }

        public void limparDados()
        {
            txtId.Text = string.Empty;
            txtNome.Text = string.Empty;
            txtEndereco.Text = string.Empty;
            mtxtCep.Text = string.Empty;
            txtBairro.Text = string.Empty;
            txtCidade.Text = string.Empty;
            txtUf.Text = string.Empty;
            mtxtTelefone.Text = string.Empty;

        }

        public bool validarDados()
        {
            if (txtNome.Text.Equals("") || txtEndereco.Text.Equals("") || mtxtCep.Text.Length != 9 || txtBairro.Text.Equals("") || txtCidade.Text.Equals("") || txtUf.Text.Length != 2 || mtxtTelefone.Text.Length != 14)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void tsbSalvar_Click(object sender, EventArgs e)
        {
            bool salvar = validarDados();

            if (!salvar)
            {
                MessageBox.Show("Falha ao cadastrar Funcionario! Verifique os dados e tente novamente.", "Erro");
            }
            else
            {
                FuncionarioRepositorio funcionarioRepo = new FuncionarioRepositorio();

                Funcionario funcionario = new Funcionario()
                {
                    nome = txtNome.Text.ToUpper(),
                    endereco = txtEndereco.Text.ToUpper(),
                    cep = mtxtCep.Text,
                    bairro = txtBairro.Text.ToUpper(),
                    cidade = txtCidade.Text.ToUpper(),
                    uf = txtUf.Text.ToUpper(),
                    telefone = mtxtTelefone.Text


                };

                bool retorno = funcionarioRepo.salvarFuncionario(funcionario);

                if(retorno)
                {
                    MessageBox.Show("Funcionario cadastrado com sucesso!");
                    limparDados();
                }
            }                      
        }

        private void tsbPesquisar_Click(object sender, EventArgs e)
        {

            if (tstIdBuscar.Text == string.Empty)
            {
                MessageBox.Show("Você precisa digitar um Id!", "Aviso");

            }
            else
            {

                int idBuscado = int.Parse(tstIdBuscar.Text);

                Funcionario funcionario = new Funcionario();
                FuncionarioRepositorio funcionarioRepo = new FuncionarioRepositorio();

                funcionario = funcionarioRepo.BuscarFuncionario(idBuscado);

                if (funcionario != null)
                {
                    txtId.Text = funcionario.id.ToString();
                    txtNome.Text = funcionario.nome.ToString();
                    txtEndereco.Text = funcionario.endereco.ToString();
                    mtxtCep.Text = funcionario.cep.ToString();
                    txtBairro.Text = funcionario.bairro.ToString();
                    txtCidade.Text = funcionario.cidade.ToString();
                    txtUf.Text = funcionario.uf.ToString();
                    mtxtTelefone.Text = funcionario.telefone.ToString();
                }
                else
                {
                    MessageBox.Show("Nenhum funcionario encontrado!", "Pesquisa");
                    limparDados();
                }               
            }           
        }

        private void tsbAlterar_Click(object sender, EventArgs e)
        {
            
            bool alterar = validarDados();

            if (!alterar)
            {
                MessageBox.Show("Falha ao alterar Funcionario! Verifique os dados e tente novamente.", "Erro");
            }
            else
            {
                
                Funcionario funcionario = new Funcionario();
                FuncionarioRepositorio funcionarioRepo= new FuncionarioRepositorio();

                funcionario.id = int.Parse(txtId.Text);
                funcionario.nome = txtNome.Text.ToUpper();
                funcionario.endereco = txtEndereco.Text.ToUpper();
                funcionario.cep = mtxtCep.Text;
                funcionario.bairro = txtBairro.Text.ToUpper();
                funcionario.cidade = txtCidade.Text.ToUpper();
                funcionario.uf = txtUf.Text.ToUpper();
                funcionario.telefone = mtxtTelefone.Text;

                bool retorno = funcionarioRepo.alterarFuncionario(funcionario);

                if (retorno)
                    MessageBox.Show("Dados de Funcionario alterados com sucesso!", "Sucesso");
            }
        }

        private void tsbCancelar_Click(object sender, EventArgs e)
        {
            limparDados();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            if(txtId.Text == String.Empty)
            {
                MessageBox.Show("Selecione um funcionário antes de excluí-lo!!", "Erro");
            }
            else
            {
                if (MessageBox.Show("Tem certeza que deseja excluir este funcionário?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    FuncionarioRepositorio funcionarioRepo = new FuncionarioRepositorio();

                    bool retorno = funcionarioRepo.excluirFuncionario(int.Parse(txtId.Text));

                    if(retorno)
                    {
                        MessageBox.Show("Funcionário Excluído com sucesso!", "Sucesso!");
                        limparDados();
                    }
                }
                
                
            }
        }

        private void tsbNovo_Click(object sender, EventArgs e)
        {
            limparDados();
            tstIdBuscar.Text = "";
        }
    }
}
