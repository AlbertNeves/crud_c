﻿using crud.Entidades;
using Crud;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crud.Repositorios
{
    public class UsuarioRepositorio
    {
        string strSql = string.Empty;
        public bool criarConta(Usuario usuario)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "insert into usuarios (usuario, senha) values (@usuario, @senha)";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);
            comando.Parameters.Add("@usuario", SqlDbType.VarChar).Value = usuario.nomeUsuario;
            comando.Parameters.Add("@senha", SqlDbType.VarChar).Value = usuario.senha;

            try
            {
                sqlCon.Open();

                comando.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Erro");
                return false;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public bool logar(Usuario usuario)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "select * from usuarios where usuario = @usuario and senha = @senha";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);
            comando.Parameters.Add("@usuario", SqlDbType.VarChar).Value = usuario.nomeUsuario;
            comando.Parameters.Add("@senha", SqlDbType.VarChar).Value = usuario.senha;

            int count = 0;

            try
            {
                sqlCon.Open();

                using (var reader = comando.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        count++;
                    }
                }
                if (count == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public string verificarSeNomeUsuarioExiste(Usuario usuario)
        {

            int count = 0;
            string retorno = string.Empty;

            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "select usuario from usuarios where usuario = @usuario";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);
            comando.Parameters.Add("@usuario", SqlDbType.VarChar).Value = usuario.nomeUsuario;

            try
            {
                sqlCon.Open();

                using (var reader = comando.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        count++;
                    }
                }

                if (count > 0)
                {
                    retorno = "Este nome de usuário já existe. Tente um diferente!";
                }
                else
                {
                    retorno = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                retorno = "Erro";
            }
            finally
            {
                sqlCon.Close();
            }
            return retorno;
        }
    }
}
