﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using crud.Consultas;
using crud.Entidades;
using Crud;

namespace crud.Repositorios
{
    class VendaProdutoRepositorio
    {
        string strSql = string.Empty;
        public bool inserirVendaProduto(VendaProduto vendaProduto)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "Insert into venda_produto (codigo_venda, codigo_produto, quantidade, total_produto) values (@codigoVenda, @codigoProduto, @quantidade, @totalProduto)";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@codigoVenda", SqlDbType.Int).Value = vendaProduto.codigoVenda;
            comando.Parameters.Add("@codigoProduto", SqlDbType.Int).Value = vendaProduto.codigoProduto;
            comando.Parameters.Add("@quantidade", SqlDbType.Float).Value = vendaProduto.quantidade;
            comando.Parameters.Add("@totalProduto", SqlDbType.Float).Value = vendaProduto.totalProduto;

            sqlCon.Open();

            try
            {
                comando.ExecuteNonQuery();
                comando.Parameters.Clear();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public List<VendaProdutoConsulta> recuperarRelatorioProdutos(int codigoVenda)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = " select p.nome, p.preco, vp.quantidade " +
                    "from venda_produto vp inner join produtos p " +
                    "on vp.codigo_produto = p.codigo where codigo_venda = @codigoVenda";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);
            comando.Parameters.Add("@codigoVenda", SqlDbType.Int).Value = codigoVenda;

            var listaProdutosDaVenda = new List<VendaProdutoConsulta>();
            try
            {
                sqlCon.Open();

                using (var reader = comando.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var vendaProduto = new VendaProdutoConsulta();

                        //vendaProduto.(reader["codigo_produto"].ToString());
                        vendaProduto.produto.nomeProduto = reader["nome"].ToString();
                        vendaProduto.produto.preco = float.Parse(reader["preco"].ToString());
                        vendaProduto.quantidade = float.Parse(reader["quantidade"].ToString());

                        listaProdutosDaVenda.Add(vendaProduto);
                    }
                }
                return listaProdutosDaVenda;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                sqlCon.Close();
            }
        }
    }
}
