﻿using crud.Consultas;
using crud.Entidades;
using Crud;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crud.Repositorios
{
    public class VendaRepositorio
    {
        string strSql = string.Empty;
        public bool criarVenda(Venda venda)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "Insert into venda (nome_cliente, observacao, total) output inserted.codigo_venda values (@cliente, @observacao, @total)";

            var gravou = false;

            sqlCon.Open();
            SqlTransaction tran = sqlCon.BeginTransaction();

            try
            {
                SqlCommand comando = new SqlCommand(strSql, sqlCon);

                comando.Parameters.Add("@cliente", SqlDbType.VarChar).Value = venda.nomeCliente;
                comando.Parameters.Add("@observacao", SqlDbType.VarChar).Value = venda.observacao;
                comando.Parameters.Add("@total", SqlDbType.Float).Value = venda.total;

                comando.Transaction = tran;

                var codigoVenda = comando.ExecuteScalar();

                comando.Parameters.Clear();

                for (int i = 0; i < venda.produtos.Count; i++)
                {
                    var strSqlProduto = "Insert into venda_produto (codigo_venda, codigo_produto, quantidade, total_produto) values (@codigoVenda, @codigoProduto, @quantidade, @totalProduto)";

                    SqlCommand comandoProduto = new SqlCommand(strSqlProduto, sqlCon);

                    comandoProduto.Parameters.Add("@codigoVenda", SqlDbType.Int).Value = codigoVenda;
                    comandoProduto.Parameters.Add("@codigoProduto", SqlDbType.Int).Value = venda.produtos[i].codigoProduto;
                    comandoProduto.Parameters.Add("@quantidade", SqlDbType.Float).Value = venda.produtos[i].quantidade;
                    comandoProduto.Parameters.Add("@totalProduto", SqlDbType.Float).Value = venda.produtos[i].totalProduto;

                    comandoProduto.Transaction = tran;

                    comandoProduto.ExecuteNonQuery();
                }

                gravou = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (gravou)
                {
                    tran.Commit();

                }
                else
                {
                    tran.Rollback();
                }
                sqlCon.Close();
            }
            return gravou;
        }

        public void atualizarTotalVenda(int vendaAtual, float total)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "update venda set total = @total where codigo_venda = @vendaAtual;";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);



            comando.Parameters.Add("@total", SqlDbType.Float).Value = total;
            comando.Parameters.Add("@vendaAtual", SqlDbType.Int).Value = vendaAtual;


            sqlCon.Open();

            try
            {
                comando.ExecuteNonQuery();
                comando.Parameters.Clear();

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                sqlCon.Close();
            }
        }

        public int recuperarIdUltimaVenda()
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "select max(codigo_venda) as codigoVenda from venda";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            int codigoVenda = 0;

            try
            {
                sqlCon.Open();

                using (var reader = comando.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();

                        codigoVenda = Convert.ToInt32(reader["codigoVenda"]);
                    }                                  
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message);
            }
            finally
            {
                sqlCon.Close();
            }
            return codigoVenda;
        }

        public VendaConsulta recuperarInfoUltimaVenda(int codigoVenda)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            VendaConsulta venda = new VendaConsulta();

            strSql = "select codigo_venda, nome_cliente, observacao, total from venda where codigo_venda = @codigo";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);
            comando.Parameters.Add("@codigo", SqlDbType.Int).Value = codigoVenda;

            try
            {
                sqlCon.Open();

                using (var reader = comando.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        venda.codigoVenda = int.Parse(reader["codigo_venda"].ToString());
                        venda.nomeCliente = reader["nome_cliente"].ToString();
                        venda.observacao = reader["observacao"].ToString();
                        venda.total = float.Parse(reader["total"].ToString());
                    }

                }
                return venda;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                sqlCon.Close();
            }
        }
    }
}
