﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using crud.Entidades;
using Crud;

namespace crud.Repositorios
{
    public class FuncionarioRepositorio
    {
        string strSql = "";
        public bool salvarFuncionario(Funcionario funcionario)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "Insert into Funcionarios (Nome, Endereco, CEP, Bairro, Cidade, UF, Telefone)" +
            " values (@Nome, @Endereco, @CEP, @Bairro, @Cidade, @UF, @Telefone)";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@Nome", SqlDbType.VarChar, 100).Value = funcionario.nome;
            comando.Parameters.Add("@Endereco", SqlDbType.VarChar, 100).Value = funcionario.endereco;
            comando.Parameters.Add("@CEP", SqlDbType.VarChar, 20).Value = funcionario.cep;
            comando.Parameters.Add("@Bairro", SqlDbType.VarChar, 50).Value = funcionario.bairro;
            comando.Parameters.Add("@Cidade", SqlDbType.VarChar, 50).Value = funcionario.cidade;
            comando.Parameters.Add("@UF", SqlDbType.Char, 2).Value = funcionario.uf;
            comando.Parameters.Add("@Telefone", SqlDbType.VarChar, 20).Value = funcionario.telefone;

            try
            {
                sqlCon.Open();
                comando.ExecuteNonQuery();

                comando.Parameters.Clear();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Falha ao inserir dados: Erro: " + ex.Message);
                return false;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public static List<Funcionario> Lista()
        {
            var lista = new List<Funcionario>();



            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            string strSql = "select Id, Nome, Endereco, CEP, Bairro, Cidade, UF, Telefone from Funcionarios where Id = @Id";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@Id", SqlDbType.Int).Value = 2;

            sqlCon.Open();

            SqlDataReader reader = comando.ExecuteReader();

            if (reader.HasRows == false)
            {
                MessageBox.Show("Nenhum resultado!");
            }
            else
            {
                MessageBox.Show(reader[0].ToString());
            }



            reader.Close();

            return lista;
        }

        public Funcionario BuscarFuncionario(int idBuscado)
        {

            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());
            strSql = "select Id, Nome, Endereco, CEP, Bairro, Cidade, UF, Telefone from Funcionarios where Id = @Id";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);
            comando.Parameters.Add("@Id", SqlDbType.Int).Value = idBuscado;

            try
            {
                sqlCon.Open();

                SqlDataReader reader = comando.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    var funcionario = new Funcionario();

                    funcionario.id = int.Parse(reader["Id"].ToString());
                    funcionario.nome = reader["Nome"].ToString();
                    funcionario.endereco = reader["Endereco"].ToString();
                    funcionario.cep = reader["CEP"].ToString();
                    funcionario.bairro = reader["Bairro"].ToString();
                    funcionario.cidade = reader["Cidade"].ToString();
                    funcionario.uf = reader["UF"].ToString();
                    funcionario.telefone = reader["Telefone"].ToString();

                    reader.Close();

                    return funcionario;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                sqlCon.Close();
            }

        }

        public List<string> ObterFuncionario(int idBuscado)
        {

            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());
            strSql = "select Id, Nome, Endereco, CEP, Bairro, Cidade, UF, Telefone from Funcionarios where Id = @Id";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);
            comando.Parameters.Add("@Id", SqlDbType.Int).Value = idBuscado;

            try
            {
                sqlCon.Open();

                SqlDataReader reader = comando.ExecuteReader();

                if (reader.HasRows == false)
                {
                    throw new Exception("Nenhum valor encontrado.");
                }

                reader.Read();

                var lista = new List<string>();

                lista.Add(reader["Id"].ToString());
                lista.Add(reader["Nome"].ToString());
                lista.Add(reader["Endereco"].ToString());
                lista.Add(reader["CEP"].ToString());
                lista.Add(reader["Bairro"].ToString());
                lista.Add(reader["Cidade"].ToString());
                lista.Add(reader["UF"].ToString());
                lista.Add(reader["Telefone"].ToString());

                reader.Close();

                return lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                sqlCon.Close();
            }

        }

        public bool alterarFuncionario(Funcionario funcionario)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "update Funcionarios set Nome = @Nome, Endereco = @Endereco, CEP = @CEP," +
                                "Bairro = @Bairro, Cidade = @Cidade, UF = @UF, Telefone = @Telefone " +
                                "where Id = @IdBuscar ";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@IdBuscar", SqlDbType.Int).Value = funcionario.id;

            comando.Parameters.Add("@Nome", SqlDbType.VarChar, 100).Value = funcionario.nome;
            comando.Parameters.Add("@Endereco", SqlDbType.VarChar, 100).Value = funcionario.endereco;
            comando.Parameters.Add("@CEP", SqlDbType.VarChar, 20).Value = funcionario.cep;
            comando.Parameters.Add("@Bairro", SqlDbType.VarChar, 50).Value = funcionario.bairro;
            comando.Parameters.Add("@Cidade", SqlDbType.VarChar, 50).Value = funcionario.cidade;
            comando.Parameters.Add("@UF", SqlDbType.Char, 2).Value = funcionario.uf;
            comando.Parameters.Add("@Telefone", SqlDbType.VarChar, 20).Value = funcionario.telefone;

            try
            {
                sqlCon.Open();

                comando.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problemas ao alterar funcionario. Erro: " + ex.Message, "Falha na alteracao");
                return false;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public bool excluirFuncionario(int idFuncionario)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "delete from Funcionarios where Id = @Id";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@Id", SqlDbType.Int).Value = idFuncionario;

            try
            {
                sqlCon.Open();
                comando.ExecuteNonQuery();

                comando.Parameters.Clear();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problemas ao excluir funcionário. Erro: " + ex.Message, "Erro");
                return false;
            }
            finally
            {
                sqlCon.Close();
            }

        }
    }
}
