﻿using crud.Entidades;
using Crud;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crud.Repositorios
{
    public class ProdutoRepositorio
    {
        string strSql = string.Empty;
        string strCon = SQLHelper.ObterConnectionString();

        public bool cadastrarProduto(Produto produto)
        {
            SqlConnection sqlCon = new SqlConnection(strCon);
            strSql = "insert into produtos(nome, marca, validade, preco, qtd_estoque) " +
                "values (@nome, @marca, @validade, @preco, @qtdEstoque)";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@nome", SqlDbType.VarChar, 100).Value = produto.nome;
            comando.Parameters.Add("@marca", SqlDbType.VarChar, 100).Value = produto.marca;
            comando.Parameters.Add("@validade", SqlDbType.Date).Value = produto.validade;
            comando.Parameters.Add("@preco", SqlDbType.Float).Value = produto.preco;
            comando.Parameters.Add("@qtdEstoque", SqlDbType.Float).Value = produto.qtdEmEstoque;

            try
            {
                sqlCon.Open();
                comando.ExecuteNonQuery();

                comando.Parameters.Clear();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problemas ao cadastrar produto. Erro: " + ex.Message, "Aviso");
                return false;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public List<Produto> listarProdutos()
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "select codigo, nome, marca, validade, preco, qtd_estoque as 'estoque' from produtos;";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            sqlCon.Open();

            var lista = new List<Produto>();

            try
            {
                using (var reader = comando.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        var produto = new Produto();
                        produto.codigo = (int)reader["codigo"];
                        produto.nome = reader["nome"].ToString();
                        produto.marca = reader["marca"].ToString();
                        produto.validade = reader["validade"].ToString();
                        produto.preco = float.Parse(reader["preco"].ToString());
                        produto.qtdEmEstoque = float.Parse(reader["estoque"].ToString());

                        lista.Add(produto);
                    }

                }

                return lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Aviso");
                return null;
            }

            finally
            {
                sqlCon.Close();
            }
        }

        public List<Produto> buscarProdutos(string produtoBuscado)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            var lista = new List<Produto>();

            //strSql = "select codigo, nome, marca, validade, preco, qtd_estoque from produtos where nome like '%@produtoBuscado%'";
            strSql = "select codigo, nome, marca, validade, preco, qtd_estoque as 'estoque' from produtos where nome like '%'+ @produtoBuscado + '%'";
            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            //comando.Parameters.AddWithValue("@produtoBuscado", "%" + produtoBuscado + "%");
            comando.Parameters.Add("@produtoBuscado", SqlDbType.VarChar).Value = produtoBuscado;

            sqlCon.Open();

            try
            {
                using (var reader = comando.ExecuteReader())
                {

                    while (reader.Read())
                    {

                        var produto = new Produto();
                        produto.codigo = (int)reader["codigo"];
                        produto.nome = reader["nome"].ToString();
                        produto.marca = reader["marca"].ToString();
                        produto.validade = reader["validade"].ToString();
                        produto.preco = float.Parse(reader["preco"].ToString());
                        produto.qtdEmEstoque = float.Parse(reader["estoque"].ToString());

                        lista.Add(produto);
                    }
                }
                return lista;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Aviso");
                return null;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public void alterarProduto(Produto produto)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "Update produtos set nome = @nome, marca = @marca, validade = @validade, preco = @preco " +
                "where codigo = @codigo";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@nome", SqlDbType.VarChar).Value = produto.nome;
            comando.Parameters.Add("@marca", SqlDbType.VarChar).Value = produto.marca;
            comando.Parameters.Add("@validade", SqlDbType.Date).Value = produto.validade;
            comando.Parameters.Add("@preco", SqlDbType.Float).Value = produto.preco;
            comando.Parameters.Add("@codigo", SqlDbType.Int).Value = produto.codigo;

            try
            {
                sqlCon.Open();

                comando.ExecuteNonQuery();

                comando.Parameters.Clear();

                MessageBox.Show("Produto alterado com sucesso!", "Sucesso");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problemas ao alterar produto. Erro: " + ex.Message);
            }
            finally
            {
                sqlCon.Close();
            }

        }

        public Produto buscarProdutoPorId(int idProdutoBuscado)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "select codigo, nome, marca, qtd_estoque as 'estoque' from produtos where codigo = @codigo";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@codigo", SqlDbType.Int).Value = idProdutoBuscado;

            try
            {
                sqlCon.Open();

                SqlDataReader reader = comando.ExecuteReader();

                if (reader.HasRows)
                {

                    reader.Read();
                    Produto produto = new Produto();

                    produto.codigo = int.Parse(reader["codigo"].ToString());
                    produto.nome = reader["nome"].ToString();
                    produto.marca = reader["marca"].ToString();
                    produto.qtdEmEstoque = float.Parse(reader["estoque"].ToString());

                    reader.Close();
                    return produto;
                }

                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message);
                return null;
            }
            finally
            {
                sqlCon.Close();
            }
        }

        public bool adicionarAoEstoque(int codigoProduto, float valorAdicional)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());
            strSql = "Update produtos set qtd_estoque = (qtd_estoque + @valorAdicional) where codigo = @codigo";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@valorAdicional", SqlDbType.Float).Value = valorAdicional;
            comando.Parameters.Add("@codigo", SqlDbType.Int).Value = codigoProduto;

            try
            {
                sqlCon.Open();

                comando.ExecuteNonQuery();

                MessageBox.Show("Estoque atualizado com sucesso!", "Sucesso!");

                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Aviso");
                return false;
            }

            finally
            {
                sqlCon.Close();
            }
        }
        public List<Produto> adicionarProdutoParaVenda(int codigoProduto)
        {

            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "select codigo, nome, preco from produtos where codigo = @codigo;";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);
            comando.Parameters.Add("@codigo", SqlDbType.Int).Value = codigoProduto;

            sqlCon.Open();

            var lista = new List<Produto>();

            try
            {
                using (var reader = comando.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        var produto = new Produto();
                        produto.codigo = (int)reader["codigo"];
                        produto.nome = reader["nome"].ToString();
                        produto.preco = float.Parse(reader["preco"].ToString());

                        lista.Add(produto);
                    }

                }

                return lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Aviso");
                return null;
            }

            finally
            {
                sqlCon.Close();
            }
        }

        public bool excluirProduto(int codigoProduto)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "delete from produtos where codigo = @codigo";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);
            comando.Parameters.Add("@codigo", SqlDbType.Int).Value = codigoProduto;

            try
            {
                sqlCon.Open();

                comando.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

            finally
            {
                sqlCon.Close();
            }
        }

        public void atualizarEstoque(int codigo, float quantidade)
        {
            SqlConnection sqlCon = new SqlConnection(SQLHelper.ObterConnectionString());

            strSql = "update produtos set qtd_estoque = (qtd_estoque - @quantidade) where codigo = @codigo";

            SqlCommand comando = new SqlCommand(strSql, sqlCon);

            comando.Parameters.Add("@quantidade", SqlDbType.Float).Value = quantidade;
            comando.Parameters.Add("@codigo", SqlDbType.Int).Value = codigo;

            try
            {
                sqlCon.Open();

                comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problemas ao atualizar estoque! Erro: " + ex, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                sqlCon.Close();
            }
        }
    }
}
