﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using crud.Entidades;
using crud.Repositorios;

namespace crud
{
    public partial class TelaProdutos : Form
    {
        public TelaProdutos()
        {
            InitializeComponent();
        }

        private void tsbNovo_Click(object sender, EventArgs e)
        {
            var novoProduto = new TelaNovoProduto();
            novoProduto.Show();
            this.Close();
        }

        private void TelaProdutos_Load(object sender, EventArgs e)
        {
            Produto produto = new Produto();
            ProdutoRepositorio produtoRepo = new ProdutoRepositorio();
            
            dgvProdutos.DataSource = produtoRepo.listarProdutos();
            dgvProdutos.Columns[0].HeaderText = "Código";
            dgvProdutos.Columns[1].HeaderText = "Nome";
            dgvProdutos.Columns[2].HeaderText = "Marca";
            dgvProdutos.Columns[3].HeaderText = "Validade";
            dgvProdutos.Columns[4].HeaderText = "Preco";
            dgvProdutos.Columns[5].HeaderText = "Estoque";

            

        }

        private void tsbBuscarProduto_Click(object sender, EventArgs e)
        {
            Produto produto = new Produto();
            ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

            string produtoBuscado = tstProdutoBuscado.Text;

            List<Produto> produtosEncontrados = produtoRepo.buscarProdutos(produtoBuscado);

            if(produtosEncontrados.Count < 1)
            {
                MessageBox.Show("Nenhum produto encontrado.", "Aviso");
                dgvProdutos.DataSource = produtoRepo.listarProdutos();
            }
            else
            {
                dgvProdutos.DataSource = produtosEncontrados;
            }

            
        }

        private void btnListaProdutos_Click(object sender, EventArgs e)
        {
            Produto produto = new Produto();
            ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

            dgvProdutos.DataSource = produtoRepo.listarProdutos();
        }

        private void tsbCancelar_Click(object sender, EventArgs e)
        {
            //var a = dgvProdutos.SelectedRows[0].Cells[0].Value.ToString();
            //MessageBox.Show(a);


        }

        private void tsbAlterar_Click(object sender, EventArgs e)
        {
            var produto = new Produto();

            produto.codigo = int.Parse(dgvProdutos.SelectedRows[0].Cells[0].Value.ToString());
            produto.nome = dgvProdutos.SelectedRows[0].Cells[1].Value.ToString();
            produto.marca = dgvProdutos.SelectedRows[0].Cells[2].Value.ToString();
            produto.validade = dgvProdutos.SelectedRows[0].Cells[3].Value.ToString();
            produto.preco = float.Parse(dgvProdutos.SelectedRows[0].Cells[4].Value.ToString());
            produto.qtdEmEstoque = float.Parse(dgvProdutos.SelectedRows[0].Cells[5].Value.ToString());

            TelaAlterarProduto telaAlterar = new TelaAlterarProduto();

            telaAlterar.txtCodigo.Text = produto.codigo.ToString();
            telaAlterar.txtNome.Text = produto.nome;
            telaAlterar.txtMarca.Text = produto.marca;
            telaAlterar.mtxtValidade.Text = produto.validade;
            telaAlterar.txtPreco.Text = produto.preco.ToString();

            telaAlterar.Show();
        }

        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            if(dgvProdutos.SelectedRows.Count == 1)
            {   

                if(MessageBox.Show("Tem certeza que deseja excluir este produto?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    int codigoProduto = int.Parse(dgvProdutos.SelectedRows[0].Cells[0].Value.ToString());
                 
                    ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

                    bool retorno = produtoRepo.excluirProduto(codigoProduto);

                    if (retorno)
                    {
                        MessageBox.Show("Produto Excluído com sucesso!");
                        dgvProdutos.DataSource = produtoRepo.listarProdutos();
                    }
                }
                
            }
            else if (dgvProdutos.SelectedRows.Count == 0)
            {
                MessageBox.Show("Selecione um produto para excluir!", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("Selecione apenas um produto por vez!", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
