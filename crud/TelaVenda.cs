﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using crud.Entidades;
using crud.Repositorios;

namespace crud
{
    public partial class TelaVenda : Form
    {

        public TelaVenda()
        {
            InitializeComponent();
            dgvProdutosSelecionados.Columns.Add("Codigo", "Codigo");
            dgvProdutosSelecionados.Columns.Add("Nome", "Nome");
            dgvProdutosSelecionados.Columns.Add("Preco", "Preco");
            dgvProdutosSelecionados.Columns.Add("Quantidade", "Quantidade");
        }

        private void btnBuscarProduto_Click(object sender, EventArgs e)
        {
            ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

            string produtoBuscado = txtProduto.Text;

            List<Produto> produtosEncontrados = produtoRepo.buscarProdutos(produtoBuscado);

            if (produtosEncontrados.Count < 1)
            {
                MessageBox.Show("Nenhum produto encontrado.", "Aviso");
                dgvProdutosBuscados.DataSource = produtoRepo.listarProdutos();
            }
            else
            {
                dgvProdutosBuscados.DataSource = produtosEncontrados;
            }


        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {         
            ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

            int codigoProduto = int.Parse(dgvProdutosBuscados.SelectedRows[0].Cells[0].Value.ToString());


            List<Produto> lista = produtoRepo.adicionarProdutoParaVenda(codigoProduto);
            

            if (float.Parse(dgvProdutosBuscados.SelectedRows[0].Cells[5].Value.ToString()) < float.Parse(txtQuantidade.Text))
            {
                MessageBox.Show("Estoque insuficiente!");
            }
            else
            {
                dgvProdutosSelecionados.Rows.Add(lista[0].codigo, lista[0].nome, lista[0].preco, txtQuantidade.Text);
            }
                     
        }

        private void btnFinalizarVenda_Click(object sender, EventArgs e)
        {     

            if (txtNomeCliente.Text == string.Empty)
            {
                MessageBox.Show("Insira o nome do cliente!");               
            }

            else
            {

                if (dgvProdutosSelecionados.RowCount > 1)
                {

                    Venda venda = new Venda
                    {
                        nomeCliente = txtNomeCliente.Text,
                        observacao = txtObservacao.Text
                        
                    };

                    //List<int> codigos = new List<int>();
                    //List<string> produtos = new List<string>();
                    //List<float> precos = new List<float>();
                    //List<float> quantidades = new List<float>();
                    
                    foreach (DataGridViewRow row in dgvProdutosSelecionados.Rows)
                    {
                        if (row.Cells[0].Value == null) {
                            continue;
                        }
                            var produto = new VendaProduto();

                        produto.codigoProduto = (int)row.Cells[0].Value;
                        produto.quantidade = float.Parse(row.Cells[3].Value.ToString());
                        produto.totalProduto = (float)row.Cells[2].Value * produto.quantidade;

                        venda.produtos.Add(produto);

                       /* if (row.Cells[1].Value != null)
                        {
                            string produto = (string)row.Cells[1].Value;

                            produtos.Add(produto.ToString());

                        }

                        if (row.Cells[2].Value != null)
                        {
                            float preco = (float)row.Cells[2].Value;

                            precos.Add(float.Parse(preco.ToString()));

                        }

                        if (row.Cells[3].Value != null)
                        {

                            string quantidade = row.Cells[3].Value.ToString();

                            quantidades.Add(float.Parse(quantidade));

                        }*/
                    }

                    //int numeroProdutos = produtos.Count;              
                    
                    VendaRepositorio vendaRepo = new VendaRepositorio();

                    bool retorno = vendaRepo.criarVenda(venda);

                    if(retorno)
                    {
                        MessageBox.Show("Venda efetuada com sucesso!", "Mensagem", MessageBoxButtons.OK,MessageBoxIcon.Information);
                        this.limparCampos();
                    }

                    //if (retorno)
                    //{
                    //    int vendaAtual = vendaRepo.recuperarVendaAtual();
                    //    float totalVenda = 0;
                    //    bool result = false;

                    //    for (int i = 0; i < numeroProdutos; i++)
                    //    {
                    //        VendaProduto vendaProduto = new VendaProduto();
                    //        VendaProdutoRepositorio vendaProdutoRepo = new VendaProdutoRepositorio();
                    //        Produto produto = new Produto();
                    //        ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

                    //        vendaProduto.codigoVenda = vendaAtual;
                    //        vendaProduto.codigoProduto = codigos[i];
                    //        vendaProduto.quantidade = quantidades[i];
                    //        vendaProduto.totalProduto = precos[i] * quantidades[i];

                    //        produto.codigo = codigos[i];

                    //        result = vendaProdutoRepo.inserirVendaProduto(vendaProduto);

                    //        produtoRepo.atualizarEstoque(produto.codigo, quantidades[i]);

                    //        totalVenda += vendaProduto.totalProduto;
                    //    }

                    //    vendaRepo.atualizarTotalVenda(vendaAtual, totalVenda);
                    //    if (result)
                    //    {
                    //        MessageBox.Show("Venda efetuada com sucesso!");
                    //        this.limparCampos();
                    //    }
                    //}

                }
                else
                {
                    MessageBox.Show("Selecione algum produto!");
                }
            }
            

            

            
        }

        private void TelaVenda_Load(object sender, EventArgs e)
        {
            
        }

        private void limparCampos()
        {
            txtNomeCliente.Text = string.Empty;
            txtObservacao.Text = string.Empty;
            txtProduto.Text = string.Empty;
            dgvProdutosBuscados.DataSource = "";
            dgvProdutosSelecionados.DataSource = "";
            txtQuantidade.Text = "1";
        }

        private void btnRelatorio_Click(object sender, EventArgs e)
        {
            TelaRelatorioVenda relatorio = new TelaRelatorioVenda();
            relatorio.Show();
        }
    }
}
