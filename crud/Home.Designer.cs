﻿namespace crud
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnTelaEstoque = new System.Windows.Forms.Button();
            this.BtnTelaVendas = new System.Windows.Forms.Button();
            this.btnTelaProdutos = new System.Windows.Forms.Button();
            this.btnTelaFuncionarios = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(273, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(456, 60);
            this.label1.TabIndex = 0;
            this.label1.Text = "ALBERT`S STORE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 24);
            this.label2.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F);
            this.label3.Location = new System.Drawing.Point(37, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 24);
            this.label3.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(433, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 36);
            this.label4.TabIndex = 7;
            this.label4.Text = "Bem vindo!";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(408, 221);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(189, 26);
            this.label5.TabIndex = 8;
            this.label5.Text = "O que deseja acessar?";
            // 
            // btnTelaEstoque
            // 
            this.btnTelaEstoque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnTelaEstoque.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnTelaEstoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTelaEstoque.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTelaEstoque.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnTelaEstoque.Image = global::crud.Properties.Resources.Box_icon;
            this.btnTelaEstoque.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTelaEstoque.Location = new System.Drawing.Point(716, 288);
            this.btnTelaEstoque.MinimumSize = new System.Drawing.Size(145, 45);
            this.btnTelaEstoque.Name = "btnTelaEstoque";
            this.btnTelaEstoque.Size = new System.Drawing.Size(150, 116);
            this.btnTelaEstoque.TabIndex = 9;
            this.btnTelaEstoque.Text = "ESTOQUE";
            this.btnTelaEstoque.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnTelaEstoque.UseVisualStyleBackColor = false;
            this.btnTelaEstoque.Click += new System.EventHandler(this.btnTelaEstoque_Click);
            // 
            // BtnTelaVendas
            // 
            this.BtnTelaVendas.BackColor = System.Drawing.Color.DarkOrange;
            this.BtnTelaVendas.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnTelaVendas.FlatAppearance.BorderSize = 5;
            this.BtnTelaVendas.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnTelaVendas.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTelaVendas.Image = global::crud.Properties.Resources.store_icon;
            this.BtnTelaVendas.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnTelaVendas.Location = new System.Drawing.Point(530, 288);
            this.BtnTelaVendas.MinimumSize = new System.Drawing.Size(145, 45);
            this.BtnTelaVendas.Name = "BtnTelaVendas";
            this.BtnTelaVendas.Size = new System.Drawing.Size(151, 116);
            this.BtnTelaVendas.TabIndex = 6;
            this.BtnTelaVendas.Text = "VENDA";
            this.BtnTelaVendas.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnTelaVendas.UseVisualStyleBackColor = false;
            this.BtnTelaVendas.Click += new System.EventHandler(this.BtnTelaVendas_Click);
            // 
            // btnTelaProdutos
            // 
            this.btnTelaProdutos.BackColor = System.Drawing.Color.Lime;
            this.btnTelaProdutos.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnTelaProdutos.FlatAppearance.BorderSize = 5;
            this.btnTelaProdutos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTelaProdutos.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTelaProdutos.Image = global::crud.Properties.Resources.bag_icon;
            this.btnTelaProdutos.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTelaProdutos.Location = new System.Drawing.Point(156, 288);
            this.btnTelaProdutos.MinimumSize = new System.Drawing.Size(145, 45);
            this.btnTelaProdutos.Name = "btnTelaProdutos";
            this.btnTelaProdutos.Size = new System.Drawing.Size(148, 116);
            this.btnTelaProdutos.TabIndex = 5;
            this.btnTelaProdutos.Text = "PRODUTOS";
            this.btnTelaProdutos.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnTelaProdutos.UseVisualStyleBackColor = false;
            this.btnTelaProdutos.Click += new System.EventHandler(this.btnTelaProdutos_Click);
            // 
            // btnTelaFuncionarios
            // 
            this.btnTelaFuncionarios.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.btnTelaFuncionarios.FlatAppearance.BorderColor = System.Drawing.Color.Fuchsia;
            this.btnTelaFuncionarios.FlatAppearance.BorderSize = 5;
            this.btnTelaFuncionarios.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTelaFuncionarios.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTelaFuncionarios.Image = global::crud.Properties.Resources.Preppy_icon;
            this.btnTelaFuncionarios.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTelaFuncionarios.Location = new System.Drawing.Point(345, 288);
            this.btnTelaFuncionarios.MinimumSize = new System.Drawing.Size(145, 45);
            this.btnTelaFuncionarios.Name = "btnTelaFuncionarios";
            this.btnTelaFuncionarios.Padding = new System.Windows.Forms.Padding(4);
            this.btnTelaFuncionarios.Size = new System.Drawing.Size(154, 116);
            this.btnTelaFuncionarios.TabIndex = 4;
            this.btnTelaFuncionarios.Text = "FUNCIONÁRIOS";
            this.btnTelaFuncionarios.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnTelaFuncionarios.UseVisualStyleBackColor = false;
            this.btnTelaFuncionarios.Click += new System.EventHandler(this.btnTelaFuncionarios_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1036, 542);
            this.Controls.Add(this.btnTelaEstoque);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BtnTelaVendas);
            this.Controls.Add(this.btnTelaProdutos);
            this.Controls.Add(this.btnTelaFuncionarios);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.Load += new System.EventHandler(this.Home_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnTelaFuncionarios;
        private System.Windows.Forms.Button btnTelaProdutos;
        private System.Windows.Forms.Button BtnTelaVendas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnTelaEstoque;
    }
}