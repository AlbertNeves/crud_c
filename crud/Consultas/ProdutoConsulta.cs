﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crud.Consultas
{
    public class ProdutoConsulta
    {
        public string nomeProduto { get; set; }
        public float preco { get; set; }
    }
}
