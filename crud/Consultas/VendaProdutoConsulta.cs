﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crud.Consultas
{
    public class VendaProdutoConsulta
    {
        public VendaProdutoConsulta()
        {
            produto = new ProdutoConsulta();           
        }

        public ProdutoConsulta produto { get; set; }
        public float quantidade { get; set; }
        public float totalProduto
        {
            get
            {
                return produto.preco * quantidade;
            }                 
        }
    }
}
