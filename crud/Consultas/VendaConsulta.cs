﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crud.Consultas
{
    public class VendaConsulta
    {
        public int codigoVenda { get; set; }
        public string nomeCliente { get; set; }
        public string observacao { get; set; }
        public float total { get; set; }
    }
}
