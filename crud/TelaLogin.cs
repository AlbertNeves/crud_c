﻿using crud.Entidades;
using crud.Repositorios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crud
{
    public partial class TelaLogin : Form
    {
        public TelaLogin()
        {
            InitializeComponent();
        }

        private void btnCriarConta_Click(object sender, EventArgs e)
        {
            TelaCadastroUsuario telaCadastro = new TelaCadastroUsuario();

            telaCadastro.Show();
            
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario
            {

                nomeUsuario = txtNomeUsuario.Text,
                senha = txtSenha.Text
            };
            UsuarioRepositorio usuarioRepo = new UsuarioRepositorio();

            bool retorno = usuarioRepo.logar(usuario);

            if(retorno)
            {
                Home home = new Home();

                home.Show();
               
            }

            else
            {
                MessageBox.Show("Usuario ou senha incorretos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
