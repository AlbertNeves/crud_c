﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using crud.Entidades;
using crud.Repositorios;

namespace crud
{
    public partial class TelaAlterarProduto : Form
    {
        public TelaAlterarProduto()
        {
            InitializeComponent();
        }

        private void TelaAlterarProduto_Load(object sender, EventArgs e)
        {
                 
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            var produto = new Produto();
            ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

            produto.codigo = int.Parse(txtCodigo.Text);
            produto.nome = txtNome.Text;
            produto.marca = txtMarca.Text;
            produto.validade = mtxtValidade.Text;
            produto.preco = float.Parse(txtPreco.Text);

            produtoRepo.alterarProduto(produto);
        }
    }
}
