﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace crud
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {

        }

        private void btnTelaFuncionarios_Click(object sender, EventArgs e)
        {
            var telaFuncionarios = new TelaFuncionarios();
            telaFuncionarios.Show();
        }

        private void btnTelaProdutos_Click(object sender, EventArgs e)
        {
            var telaProdutos = new TelaProdutos();
            telaProdutos.Show();
        }

        private void btnTelaEstoque_Click(object sender, EventArgs e)
        {
            var telaEstoque = new TelaEstoque();
            telaEstoque.Show();
        }

        private void BtnTelaVendas_Click(object sender, EventArgs e)
        {
            var telaVenda = new TelaVenda();
            telaVenda.Show();
        }
    }
}
