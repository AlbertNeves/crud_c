﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using crud.Entidades;
using crud.Repositorios;

namespace crud
{
    public partial class TelaNovoProduto : Form
    {
        private bool nonNumberEntered = false;
        public TelaNovoProduto()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            var telaProdutos = new TelaProdutos();          
            telaProdutos.Show();
            this.Close();          
            
        }

        private void TelaNovoProduto_Load(object sender, EventArgs e)
        {

        }
        public bool validarDados()
        {
            
            if (txtNome.Text.Equals("") || txtMarca.Text.Equals("") || mtxtValidade.Text.Length != 10 || txtPreco.Text.Equals(""))
            {
                return false;
            }
            
            else
            {
                return true;
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            bool validacao = validarDados();
            if (validacao)
            {
                ProdutoRepositorio produtoRepo = new ProdutoRepositorio();

                Produto produto = new Produto
                {   
                    nome = txtNome.Text,
                    marca = txtMarca.Text,
                    validade = mtxtValidade.Text,
                    preco = float.Parse(txtPreco.Text),
                    qtdEmEstoque = 0                  
                };

                bool retorno = produtoRepo.cadastrarProduto(produto);

                if (retorno)
                {
                    MessageBox.Show("Produto cadastrado com sucesso!", "Sucesso");
                }
            }
            else
            {
                MessageBox.Show("Dados inválidos! Digite os dados corretamente e tente novamente.");
            }
        }

        private void txtPreco_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    if(e.KeyCode != Keys.Oemcomma)
                    {
                        if(e.KeyCode != Keys.Back)
                            nonNumberEntered = true;
                    }
                        
                }
            }
            else
            {
                nonNumberEntered = false;
            }

            
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(nonNumberEntered)
            {
                e.Handled = true;
            }
        }

        private void mtxtValidade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    if (e.KeyCode != Keys.Back)
                        nonNumberEntered = true;
                }
            }
            else
            {
                nonNumberEntered = false;
            }
        }

        private void mtxtValidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (nonNumberEntered)
            {
                e.Handled = true;
            }
        }
    }
}
