﻿using Crud;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using crud.Entidades;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace crud.Entidades
{
    public class VendaProduto
    {     
        
        public int codigoVenda { get; set; }
        public int codigoProduto { get; set; }
        public float quantidade { get; set; }
        public float totalProduto { get; set; }       
    }
}
