﻿using Crud;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using crud.Entidades;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace crud.Entidades
{
    public class Venda
    {
        public Venda()
        {
            produtos = new List<VendaProduto>();
        }
        public int codigoVenda { get; set; }
        public string nomeCliente { get; set; }
        public string observacao { get; set; }
        public float total {
            get
            {
                return produtos.Sum(x => x.totalProduto);
            }
        }
        public List<VendaProduto> produtos {get; set;}
    }
}
