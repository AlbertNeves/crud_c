﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Crud;

namespace crud.Entidades
{
    public class Produto
    {
        public int codigo { get; set; }
        public string nome { get; set; }
        public string marca { get; set; }
        public string validade { get; set; }
        public float preco { get; set; }
        public float qtdEmEstoque { get; set; }       
    }   
}
